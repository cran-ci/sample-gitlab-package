# GitlabExample

## Project Status

[![Latest Release](https://gitlab.com/cran-ci/gitlabexample/-/badges/release.svg)](https://gitlab.com/cran-ci/gitlabexample/-/releases)
[![pipeline status](https://gitlab.com/cran-ci/gitlabexample/badges/main/pipeline.svg)](https://gitlab.com/cran-ci/gitlabexample/-/commits/main)
[![coverage report](https://gitlab.com/cran-ci/gitlabexample/badges/main/coverage.svg)](https://gitlab.com/cran-ci/gitlabexample/-/commits/main)

## Install 

### CRAN

```
install.packages("GitlabExample", repos = c("https://cran.rstudio.org"))
```


### Development Version

[![pipeline status](https://gitlab.com/cran-ci/gitlabexample/badges/develop/pipeline.svg)](https://gitlab.com/cran-ci/gitlabexample/-/commits/develop)
[![coverage report](https://gitlab.com/cran-ci/gitlabexample/badges/develop/coverage.svg)](https://gitlab.com/cran-ci/gitlabexample/-/commits/develop)


```
install.packages("GitlabExample", repos = c("https://cran-ci.gitlab.io/gitlabexample/drat/", "https://cran.rstudio.org"))
```

