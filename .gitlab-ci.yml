stages:
  - setup
  - build
  - check
  - release
  - publish
  - tag

variables:
  PKG: "${CI_PROJECT_TITLE}"
  PKGL: "${CI_PROJECT_NAME}"

setup:version:
  stage: setup
  image: rocker/verse:latest
  artifacts:
    reports:
      dotenv: variables.env
  before_script:
    - export VERSION=$(grep DESCRIPTION -e "Version:" | awk '{print $2}')
    - export CRAN_NAME=$(grep DESCRIPTION -e "Package:" | awk '{print $2}')
    - export VERSION_TIMESTAMP=$(date +%y%m%d%H%M)
    - |-
      if [ "$CI_COMMIT_REF_NAME" == "main" ] || [ -n "${CI_COMMIT_TAG}" ]; then
        export VERSION_FULL="${VERSION}"
      else
        export VERSION_FULL="${VERSION}.${VERSION_TIMESTAMP}"
      fi
  script:
    - echo "Setting up variables..."
    - echo $GITLAB_USER_LOGIN
    - echo $CI_PROJECT_TITLE
    - echo $CI_PROJECT_NAME
    - echo $CI_REGISTRY_IMAGE
    - echo $CI_REPOSITORY_URL
    - echo $CI_PROJECT_PATH
    - echo $CRAN_NAME
    - echo $VERSION
    - echo $VERSION_FULL
    - echo VERSION_TIMESTAMP="${VERSION_TIMESTAMP}" >> ./variables.env
    - echo VERSION_FULL="${VERSION_FULL}" >> ./variables.env
    - echo PACKAGE_VERSION="${CRAN_NAME}_${VERSION_FULL}" >> ./variables.env
    - cat ./variables.env

build:package:
  stage: build
  image: rocker/tidyverse:latest
  artifacts:
    reports:
      dotenv: variables.env
    paths:
      - archive/
  script:
    - apt update && apt install -y curl libxt-dev
    - echo $VERSION_FULL
    - echo $PACKAGE_VERSION
    - "sed -i -e \"s/Version: \\(.*\\)/Version: ${VERSION_FULL}/\" DESCRIPTION"
    - cat DESCRIPTION
    - Rscript -e 'update(devtools::dev_package_deps(".", repos = c("https://cran.rstudio.org")))'
    - Rscript -e 'Rcpp::compileAttributes()'
    - cd ..; R CMD build --resave-data --no-manual --log ${CI_PROJECT_NAME}
    - mkdir -p ${CI_PROJECT_NAME}/archive/
    - ls -al ./
    - ls -al ${CI_PROJECT_NAME}/archive/ || true
    - mv ./${PACKAGE_VERSION}.tar.gz ${CI_PROJECT_NAME}/archive/
    - ls -al ${CI_PROJECT_NAME}/ || true
    - ls -al ${CI_PROJECT_NAME}/archive/ || true

# Rhub Templates
.rhub_condition: &rhub_condition
  only:
    refs:
      - develop
      - main
      - $RHUB_EMAIL_ADDRESS
      - $RHUB_EMAIL_TOKEN
  except:
    refs:
      - tags
    variables:
      - $CI_COMMIT_MESSAGE =~ /.*\[skip check\].*/
.rhub_definition: &rhub_definition
  stage: check
  image: rocker/verse:latest
  artifacts:
    reports:
      dotenv: variables.env
    paths:
      - archive/
    expire_in: 30 days
  script:
    - echo $VERSION_FULL
    - echo $PACKAGE_VERSION
    - cat DESCRIPTION
    - "sed -i -e \"s/Version: \\(.*\\)/Version: ${VERSION_FULL}/\" DESCRIPTION"
    - cat DESCRIPTION
    - Rscript -e 'install.packages(c("rhub"), repos = c("https://cran.rstudio.org"))'
    - Rscript -e 'Rcpp::compileAttributes(verbose = TRUE)'
    - echo "Full Version - ${VERSION_FULL}"
    - echo "CRAN Name - ${PACKAGE_VERSION}"
    - ls -al ./archive/ || true
    - mv ./archive/ ../archive/
    - ls -al ./
    - ls -al ./archive || true
    - rm -rf ./archive/ || true
    - ls -al ./
    - ls -al ./archive/ || true
    - ls -al ../archive/ || true
    - |-
        Rscript -e "Rcpp::compileAttributes(verbose = TRUE);
                    rhub::validate_email(email = \"${RHUB_EMAIL_ADDRESS}\", token = \"${RHUB_EMAIL_TOKEN}\");
                    checked <- rhub::check_for_cran(platforms = \"${RHUB_PLATFORM}\", show_status = TRUE );
                    checked\$cran_summary();
                    print(paste0('Ref:', Sys.getenv('CI_COMMIT_REF_NAME')));
                    print(paste0('Ext:', Sys.getenv('RHUB_EXT')));
                    if (Sys.getenv('CI_COMMIT_REF_NAME') != 'main' && Sys.getenv('RHUB_EXT') != '') httr::GET(url = paste0(checked\$urls()\$artifacts, '', \"${PACKAGE_VERSION}.${RHUB_EXT}\"), httr::write_disk(\"../archive/${PACKAGE_VERSION}-${RHUB_PLATFORM}.${RHUB_EXT}\", overwrite = TRUE));
                    quit(status = ifelse(!checked\$.__enclos_env__\$private\$status_[[1]]\$status %in% c('error', 'warning'), 0, 1));";
    - mv ../archive/ ./archive/
  <<: *rhub_condition

# Rhub Checks
check:r-hub-windows-release:
  variables:
    RHUB_EXT: "zip"
    RHUB_PLATFORM: "windows-x86_64-release"
  <<: *rhub_definition
check:macos-highsierra-release-cran:
  variables:
    RHUB_EXT: "tgz"
    RHUB_PLATFORM: "macos-highsierra-release-cran"
  <<: *rhub_definition

# Site updates
publish:
  stage: publish
  image: registry.gitlab.com/cran-ci/gitlabexample/pkgdown:latest
  only:
    refs:
      - develop
      - tags
  except:
    refs:
      - main
  artifacts:
    reports:
      dotenv: variables.env
    paths:
      - archive/
    expire_in: 30 days
  script:
    # - git status
    - git remote -v
    - git fetch origin pages
    - git status
    - git checkout pages
    - tar --strip-components=1 -xvf archive/${PACKAGE_VERSION}.tar.gz
    - cat DESCRIPTION
    - ls -al ./ || true
    - ls -al ./archive/ || true
    - Rscript -e 'update(devtools::dev_package_deps(".", repos = c("https://cran.rstudio.org")))'
    - Rscript -e 'Rcpp::compileAttributes()'
    - Rscript -e "devtools::install(\".\", quick = T, build = T, dependencies = F, upgrade = F)"
    - |-
        if [ "$CI_COMMIT_REF_NAME" -ne "main" ]; then
          Rscript -e 'pkgdown::build_site( override = list( destination = "public/develop/") )';
        else
          Rscript -e 'pkgdown::build_site( override = list( destination = "public/") )';
        fi;
    - ls -al ./
    - ls -al ./archive/ || true
    - ls -al ./public/ || true
    - ls -al ./public/develop/ || true
    - |-
        if [ "$CI_COMMIT_REF_NAME" -ne "main" ]; then
          export TAR_FILE=`ls -A1 ./archive/ | grep tar.gz`;
          export TGZ_FILES=`ls -A1 ./archive/ | grep -E ".*(\.zip|\.tgz)"`;
          echo "TAR- $TAR_FILE";
          echo "TGZ- $TGZ_FILES";
          Rscript -e "drat::insertPackage(file = './archive/${TAR_FILE}', repodir = './public/drat/')";
          readarray -t A <<<"$TGZ_FILES";
          for i in "${A[@]}"; do \
            export B=`echo $i | sed -E 's/([^-]*)-[^\.]*(\.zip|\.tgz)/\1\2/'`; \
            echo "FILE TO INSERT: $B"
            mv ./archive/$i ./archive/$B; \
            ls -al ./archive/; \
            Rscript -e "drat::insertPackage(file = './archive/$B', repodir = './public/drat/')"; \
            rm ./archive/$B; \
          done;
          Rscript -e "drat::pruneRepo(repopath = './public/drat/', remove = TRUE)";
          cd public/drat/src/contrib; tree -H './src/contrib' --noreport -h --charset utf-8 -T ${CI_PROJECT_TITLE} -o ../../../packages.html;
          cd ../../../../;
        fi
    - ls -al ./archive/;
    - mkdir -p ./public/coverage;
    - ls -al ./public/coverage/ || true
    - |-
        Rscript -e "cov <- covr::package_coverage(quiet = F, function_exclusions = c('.onLoad'));
                    print(cov);
                    rep <- ifelse(Sys.getenv(\"CI_COMMIT_REF_NAME\") != 'main', covr::report( x = cov, file = \"./public/develop/coverage/index.html\", browse = FALSE), covr::report( x = cov, file = \"./public/coverage/index.html\", browse = FALSE ));"
    - |-
        git status;
        git config --global user.name "${GITLAB_USER_NAME}";
        git config --global user.email "${GITLAB_USER_EMAIL}";
        git add public/;
        CHANGES=$(git status --porcelain | wc -l);
        if [ "$CHANGES" -gt "0" ]; then
          git commit -m "updated pages for ${CI_COMMIT_REF_NAME} (${CI_COMMIT_SHORT_SHA})";
          git remote rm origin && git remote add origin https://gitlab.com/${CI_PROJECT_PATH}.git;
          git status;
          git push https://${GITLAB_USER_LOGIN}:${CI_PAT}@gitlab.com/${CI_PROJECT_PATH}.git HEAD:pages;
        fi
    - echo ${CI_PAGES_URL}

# CRAN
.cran_comments: &cran_comments
  before_script:
    - rm -f cran-comments.md
    - touch cran-comments.md
    - echo $CI_COMMIT_MESSAGE > cran-comments.md
    #- sed -n '/# ${CI_PROJECT_TITLE}/,/# ${CI_PROJECT_TITLE}/p' NEWS.md >> cran-comments.md
    - echo "$(cat NEWS.md | sed 's/\(dev[^)]*\)/0.1.0/')" | sed -n "/# ${CI_PROJECT_TITLE}/,/# ${CI_PROJECT_TITLE}/p" >> cran-comments.md
    - cat ./cran-comments.md || true
    
.release_build_definition: &release_build_template
  <<: *cran_comments
  # before_script:
  #   - export _R_CHECK_CRAN_INCOMING_="true"
  #   - export _R_CHECK_CRAN_INCOMING_REMOTE_="true"
  #   - export _R_CHECK_FORCE_SUGGESTS_="true"
  #   - export _R_CLASS_MATRIX_ARRAY_="true"
  #   - export R_REMOTES_STANDALONE="true"
  #   - export R_REMOTES_NO_ERRORS_FROM_WARNINGS="true"
  #   - export TZ=Europe/London
  #   - export _R_CHECK_LENGTH_1_CONDITION_="package:_R_CHECK_PACKAGE_NAME_,verbose"
  #   - cat DESCRIPTION
  #   - R -e 'Rcpp::compileAttributes()'
  #   - R -e 'update(devtools::dev_package_deps("."))'
  #   - R -e 'devtools::install_dev_deps()'
  #   - R -e 'devtools::install_deps()'
  #   - cd ..; ls -al; R CMD build --resave-data --no-manual --log ${CI_PROJECT_NAME}
  #   - ls -al;
.release_definition: &release_definition
  stage: release
  image: rocker/verse:latest
  only:
    refs:
      - main
  <<: *release_build_template
  script:
    - ls -al
    - cat ./cran-comments.md || true
    # - touch cran-comments.md
    # - echo $CI_COMMIT_MESSAGE > cran-comments.md
    # - echo $(sed -n '/# GitlabExample/,/# GitlabExample/{//!p;}' NEWS.md) >> cran-comments.md
    # - cat ./cran-comments.md || true
    - ls -al || true
    #- Rscript -e "devtools:::upload_cran(devtools::as.package(\"${PKG}\"), \"./${PKG}_${VERSION}.tar.gz\")"
    #- "curl --request POST --header \"PRIVATE-TOKEN: ${GITLAB_API_TOKEN}\" --data-urlencode \"tag_name=${VERSION}\" --data-urlencode \"name=v${VERSION}\" --data-urlencode \"description=Automatically created from CI for ${VERSION}\" \"${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/releases?ref=${CI_COMMIT_REF_NAME}\""
  allow_failure: false
release:cran-manual:
  <<: *release_definition
  # when: manual
  
  
# Tag release
.tag_build_definition: &tag_build_template
  <<: *cran_comments

.tag_definition: &tag_definition
  stage: tag
  image: rocker/verse:latest
  only:
    refs:
      - main
  <<: *tag_build_template
  script:
    - echo "$(cat NEWS.md | sed 's/\(dev[^)]*\)/0.1.0/')" > NEWS.md
    - printf '%s\n' "# ${CI_PROJECT_TITLE} (development version)" "" "* TBD" "" "$(cat NEWS.md)"
    - cat NEWS.md
    - "curl --request POST --header \"PRIVATE-TOKEN: ${GITLAB_API_TOKEN}\" --data-urlencode \"tag_name=${VERSION_FULL}\" --data-urlencode \"name=${VERSION_FULL}\" --data-urlencode \"description=${TAG_MESSAGE}\" \"${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/releases?ref=${CI_COMMIT_REF_NAME}\""
  allow_failure: false
tag:manual:
  <<: *tag_definition
  when: manual